import React, { Component } from 'react';
import FormLogin from '../../components/form-login/FormLogin';
import voxusLogo from './imgs/voxus-logo.png';
import voxusLogoFooter from './imgs/voxus-logo-footer.png';
import '../login/Login.css';
class Login extends Component {
    render() {
        return (
            <section className="login_page">
                <header className="login--header" style={{
                    height: '50px',
                    display: 'flex',
                    alignItems: 'center',
                    paddingLeft: '20px',
                    background: 'white',
                    width: '100%'
                }}>
                    <img className="login--logo_header" src={voxusLogo} alt="Voxus Logo"/>
                </header>
                <main className="login--main_content">
                    <article>
                        <h1 className="main_content--heading">Você está a poucos passos de otimizar suas campanhas!</h1>
                        <h2 className="main_content--subheading">Queremos conhecer mais sobre sua empresa para melhorar sua experiência com a Voxus.</h2>
                    </article>
                    <FormLogin />
                </main>
                {/* I don't know why style are not full been rendered on footer */}
                <footer className="login_page--footer">
                    <div className="login--logo_footer">
                        <img  src={voxusLogoFooter} alt="Voxus Logo" />
                    </div>
                    <ul>
                        <li className="login--footer-list">
                            <a className="login--footer_link" href="/#">Termos de serviço</a>
                        </li>
                        <li className="login--footer-list">
                            <a className="login--footer_link" href="/#">Política de privacidade</a>
                        </li>
                        <li className="login--footer-list">
                            <a className="login--footer_link" href="/#">Opções de anúncio</a>
                        </li>

                    </ul>
                </footer>
            </section>
        );
    }
}

export default Login;