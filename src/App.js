import React from 'react';
import LoginPage from './pages/login/Login'

function App() {
	return (
		<div className="main_container" style={{
			backgroundColor: '#E5E5E5',
			backgroundImage: "url('./pages/login/imgs/Illustration.png')",
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'bottom center',
		}}>
			<LoginPage />
		</div>
  	);
}

export default App;
