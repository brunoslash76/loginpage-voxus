import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Input.css';

class Input extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hasError: false,
            hasFocus: false,
            inputValue: '',
            isPristine: true,
            isValid: false,
        };
        this.handleInputFocus = this.handleInputFocus.bind(this);
        this.handleInputBlur = this.handleInputBlur.bind(this);
        this.handleUpdateInputValue = this.handleUpdateInputValue.bind(this);
    }

    componentDidMount() {
        this.setState({isValid: this.props.isValid})
    }


    handleInputFocus(e) {
        e.persist();
        this.setState({
            hasFocus: true,
        });
    }

    handleInputBlur(e) {
        e.persist();
        if (e.target.value !== '') {
            return;
        }
        this.setState({ 
            hasFocus: false,
            isPristine: false 
        });
    }

    handleUpdateInputValue(e) {
        e.persist();
        this.setState({ 
            inputValue: e.target.value,
            isValid: e.target.value !== '' ? true : false,
        });
    }

    render() {
        // debugger
        console.log(!this.props.isFormValid && !this.state.isValid  && this.props.shouldSubmit);
        return (
            <section className="input--container" style={this.props.marginController ? { margin: '0', width: '100%' } : {}}>
                <label htmlFor={this.props.name} className={`label ${this.state.hasFocus ? '--up' : ''}`}>
                    {this.props.title}
                </label>
                <input
                    name={this.props.name}
                    id={this.props.name}
                    className="input"
                    type={this.props.type}
                    style={{ boderRadius: this.props.roundedBorders ? this.props.roundedBorders : '3px' }}
                    onFocus={this.handleInputFocus}
                    onBlur={this.handleInputBlur}
                    onChange={this.handleUpdateInputValue}
                    value={this.state.inputValue}
                />
                <div className={`error-container ${!this.props.isFormValid && !this.props.isValid && this.props.shouldSubmit ? 'has-error' : ''}`}>{this.props.errorMessage}</div>
            </section>
        );
    }
}

Input.propTypes = {
    name: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    isFormValid: PropTypes.bool.isRequired,
    isValid: PropTypes.bool.isRequired,
    shouldSubmit: PropTypes.bool.isRequired,
    roundedBorders: PropTypes.string,
    marginController: PropTypes.bool,
    errorMessage: PropTypes.string,
};

export default Input;