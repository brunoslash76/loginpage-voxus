import React, { Component } from 'react';
import PropTypes from 'prop-types';
import arrow from './img/arrow.png';

import './Select.css';

class Select extends Component {

    constructor(props) {
        super(props);
        this.state = {
            shouldTurnArrow: false,
            selectedOption: {},
            shouldShowDropbox: false,
            defaultOption: {},
            isPristine: true,
            didSelect: false,
        }

        this.handleSelectChange = this.handleSelectChange.bind(this);
        this.handleOptionSelect = this.handleOptionSelect.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
    }

    componentDidMount() {
        this.setState({
            selectedOption: this.props.options[0],
            defaultOption: this.props.defaultOption,
        });
    }

    handleSelectChange(e) {
        e.persist();
        e.preventDefault();
        let shouldRotate = true;
        if (e.type === 'focus') {
            shouldRotate = false;
        }
        this.setState({ shouldTurnArrow: shouldRotate });
    }

    handleOptionSelect(option) {
        this.setState({
            selectedOption: option,
            didSelect: true,
            shouldShowDropbox: false,
        })
    }

    handleSelect() {
        this.setState({
            shouldShowDropbox: !this.state.shouldShowDropbox,
            shouldTurnArrow: !this.state.shouldTurnArrow,
            isPristine: false,
        })
    }

    render() {
        return (
            <section
                className="select--container"
                style={this.props.hasInlineSibling
                    ? { margin: '0', width: this.props.width }
                    : {}
                }
            >
                <img src={arrow} alt=""
                    className={`container--arrow ${this.state.shouldTurnArrow ? '--up' : ''}`}
                    style={{ ...this.props.arrowDistance, pointerEvents: 'none' }}
                />
                <div className="select" style={this.props.hasInlineSibling ? {} : { width: '100%' }}>

                    <div className="select--default" 
                        onClick={this.handleSelect} 
                        style={this.state.isPristine && !this.state.didSelect ? {color: `rgba(45, 58, 64, 0.3)`} : {}}
                    >
                        {
                            this.state.selectedOption.name
                                ? this.state.selectedOption.name
                                : (
                                    <img src={this.state.selectedOption.img} alt=""
                                        style={{ position: 'absolute', right: '25px' }} 
                                    />
                                )
                        }
                    </div>
                    <div>
                        <ul className={`dropbox ${this.state.shouldShowDropbox ? '--show' : ''}`} name={this.props.name}>
                            {this.props.options.map((option, index) => {
                                if (option.id !== this.state.defaultOption.id) {
                                    return (
                                        <li key={index}
                                            onClick={() => this.handleOptionSelect(option)}
                                            style={this.state.isPristine ? {} : { 
                                                display: 'flex', 
                                                color: '#2D3A40', 
                                                justifyContent: 'flex-start',
                                                paddingLeft: '20px'
                                            }}
                                        >
                                            {option.name ? option.name : (<img src={option.img} alt="flag" />)}
                                        </li>)
                                }
                            })}
                        </ul>
                        <input type="text" id={this.props.name} name={this.props.name} value={this.state.selectedOption.value} style={{display: 'none'}}/>
                    </div>
                </div>

            </section>
        );
    }
}

Select.propTypes = {
    options: PropTypes.array.isRequired,
    borderRadius: PropTypes.string,
    hasInlineSibling: PropTypes.bool,
    width: PropTypes.string,
    defaultOption: PropTypes.object,
    name: PropTypes.string.isRequired,
}

export default Select;