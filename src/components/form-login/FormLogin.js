import React, { Component } from 'react';
import Input from '../inputs/Input/Input';
import Select from '../inputs/Select/Select';

import './FormLogin.css'

import brflag from './img/brazil.png';
import usaflag from './img/united-states.png';
import deflag from './img/germany.png';

class FormLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            formData: {
                fullname: {
                    value: '',
                    isValid: false,
                    isVerified: false,
                    name: 'fullname',
                    required: true,
                },
                country: {
                    value: '',
                    isValid: true,
                    isVerified: true,
                    name: 'country',
                    required: false,
                },
                telephone: {
                    value: '',
                    isValid: false,
                    isVerified: false,
                    name: 'telephone',
                    required: true,
                },
                email: {
                    value: '',
                    isValid: false,
                    isVerified: false,
                    name: 'email',
                    required: true,
                },
                company: {
                    value: '',
                    isValid: false,
                    isVerified: false,
                    name: 'company',
                    required: true,
                },
                site: {
                    value: '',
                    isValid: false,
                    isVerified: false,
                    name: 'site',
                    required: true,
                },
                budget: {
                    value: '',
                    isValid: true,
                    isVerified: true,
                    name: 'budget',
                    required: false,
                }
            },
            isFormValid: false,
            buttonMessage: 'Experimente grátis',
            shouldVerify: false,
            formSuccess: false,
            shouldSubmit: false,
        };
        this.handleSumbit = this.handleSumbit.bind(this);
    }

    validateField(field) {
        // debugger;
        const regex = /[\t\r\n]|(--[^\r\n]*)|(\/\*[\w\W]*?(?=\*)\*\/)/gi;

        if (!field.required) {
            field.isValid = true;
            field.isVerified = true;
        }

        if (field.value !== '' && field.required) {
            field.isValid = true;
            field.isVerified = true;
        }

        if(field.value !== '' && !regex.test(field.value) && field.required) {
            field.isValid = true;
            field.isVerified = true;
        } 

        if(field.name === 'telephone') {
            field.isValid = this.verifyTelephone(field.value);
            field.isVerified = true;
        }

        if (field.name === 'site') {
            field.isValid = this.verifySite(field.value);
            field.isVerified = true;
        }

        if (field.name === 'email') {
            // debugger;
            field.isValid = this.verifyEmail(field.value);
            field.isVerified = true;
        }

        return field;

    }

    verifyTelephone(telephone) {
        const regex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g
        return telephone === '' ? false : this.verifyString(telephone, regex);
    }

    verifyEmail(email) {
        const regular = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        // I am not understanding why regex test is not working, so I will fake the result
        console.log(email)
        return email === '' ? false : this.verifyString(String(email).toLowerCase(), regular);
    }

    verifySite(site) {
        const regex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
        return site === '' ? false : this.verifyString(site, regex);
    }

    verifyString(string, regex) {
        return regex.test(string);
    }

    handleSumbit(e) {
        e.preventDefault();
        e.persist();

        let formData = {
            fullname: {
                value: e.target[0].value,
                isValid: false,
                isVerified: false,
                name: 'fullname',
                required: true,
            },
            country: {
                value: e.target[1].value,
                isValid: false,
                isVerified: false,
                name: 'country',
                required: false,
            },
            telephone: {
                value: e.target[2].value,
                isValid: false,
                isVerified: false,
                name: 'telephone',
                required: true,
            },
            email: {
                value: e.target[3].value,
                isValid: false,
                isVerified: false,
                name: 'email',
                required: true,
            },
            company: {
                value: e.target[4].value,
                isValid: false,
                isVerified: false,
                name: 'company',
                required: true,
            },
            site: {
                value: e.target[5].value,
                isValid: false,
                isVerified: false,
                name: 'site',
                required: true,
            },
            budget: {
                value: e.target[6].value,
                isValid: false,
                isVerified: false,
                name: 'budget',
                required: false,
            }
        }
        let isFormValid = true;
        Object.keys(formData).forEach(el => {
            const field = this.validateField(formData[el]);
            if (!field.isValid) {
                isFormValid = false;
            }
            formData[el] = field;
        });

        console.log(formData)

        this.setState({formData, isFormValid, shouldVerify: true}, () => {
            if (this.state.isFormValid) {
                this.setState({
                    buttonMessage: 'Obrigado!',
                    formSuccess: true,
                    shouldSubmit: true,
                })
            }
        });
    }

    render() {
        console.log(this.state.formData)
        return (
            <form className="form_login" onSubmit={this.handleSumbit} name="formLogin">
                <Input
                    shouldSubmit={this.state.shouldSubmit}
                    isFormValid={this.state.isFormValid}
                    name="fullname"
                    title="Nome completo"
                    type="text"
                    isValid={this.state.formData.fullname.isVerified && this.state.formData.fullname.isValid}
                    errorMessage="Este campo é obrigatório"
                />
                <div className="form_input--inputs--aling_inputs">
                    <Select
                        name="country"
                        options={[
                            { id: '1', name: null, value: '+55', img: brflag },
                            { id: '2', name: null, value: '+1', img: usaflag },
                            { id: '3', name: null, value: '+44', img: deflag },
                        ]}
                        roundedBorders="3px 0 0 3px"
                        width="61px"
                        hasInlineSibling={true}
                        defaultOption={{ name: null, value: '+55', img: brflag }}
                    />
                    <Input
                        shouldSubmit={this.state.shouldSubmit}
                        isFormValid={this.state.isFormValid}
                        name="telephone"
                        title="Telefone"
                        type="telephone"
                        roundedBorders="0 3px 3px 0"
                        marginController={true}
                        isValid={this.state.formData.telephone.isVerified && this.state.formData.telephone.isValid}
                        errorMessage="Telefone inválido"
                    />
                </div>
                <Input
                    shouldSubmit={this.state.shouldSubmit}
                    isFormValid={this.state.isFormValid}
                    name="email"
                    title="Endereço de email"
                    type="email"
                    isValid={this.state.formData.email.isVerified && this.state.formData.email.isValid}
                    errorMessage="E-mail inválido"
                />
                <Input
                    shouldSubmit={this.state.shouldSubmit}
                    isFormValid={this.state.isFormValid}
                    name="company"
                    title="Nome da empresa"
                    type="text"
                    isValid={this.state.formData.company.isVerified && this.state.formData.company.isValid && this.state.shouldVerify}
                    errorMessage="Este campo é obrigatório"
                />
                <Input
                    shouldSubmit={this.state.shouldSubmit}
                    isFormValid={this.state.isFormValid}
                    name="site"
                    title="URL do site"
                    type="text"
                    isValid={this.state.formData.site.isVerified && this.state.formData.site.isValid}
                    errorMessage="Domínio inválido"
                />
                <Select
                    name="budget"
                    options={[
                        { id: '1', name: 'Verba mensal de mídia', value: '' },
                        { id: '2', name: '<= R$ 5.000', value: '<= 5000' },
                        { id: '3', name: '>= R$ 5.001 e <= R$ 30.000', value: '>= 5001 <= 30000' },
                        { id: '4', name: '>= R$ 30.001 e <= R$ 100.000', value: '>= 30001 <= 100000' },
                        { id: '5', name: '>= R$ 100.001 e <= R$ 500.000', value: '>= 100001 <= 500000' },
                        { id: '6', name: '>= R$ 500.001', value: '>= 500001' },
                        { id: '7', name: 'Não se aplica', value: '' },
                    ]}
                    defaultOption={{ id: '1', name: 'Verba mensal de mídia', value: '' }}
                />

                <div className="button-container">
                    <button className={`btn-primary ${this.state.isFormValid ? '--outline' : ''}`}>{this.state.buttonMessage}</button>
                </div>

                {
                    this.state.formSuccess 
                        ? (<div className="form--succes_notification">Entraremos em contato em até 24hrs úteis</div>) 
                        : null
                }


            </form>
        );
    }
}

export default FormLogin;